package com.exam.s20.stringrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StringRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StringRestApiApplication.class, args);
	}

}
