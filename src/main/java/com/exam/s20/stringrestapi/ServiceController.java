package com.exam.s20.stringrestapi;
import java.util.HashSet;
import java.util.Set;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class ServiceController {
    @GetMapping("/string-reverse")
    public String reverse(@RequestParam(value="string")String string) {
        StringBuilder strBuilder = new StringBuilder();
        char[] strChars = string.toCharArray();

        for (int i = strChars.length - 1; i >= 0; i--) {
            strBuilder.append(strChars[i]);
        }

        return strBuilder.toString();
    }
    @GetMapping("/palindrome")
    public String isPalindrome1(@RequestParam(value="str")String input) {
        String result = "";
        StringBuilder stringBuilder = new StringBuilder(input);

        if(input.equals(stringBuilder.reverse().toString()) == true){
            result = "Đây là chuỗi đảo ngược";
        } else
        {
            result = "Đây không phải là chuỗi đảo ngược";
        }
        return result;
    }
    @GetMapping("/chars")
    public String removeDuplicateChar(@RequestParam(value="str")String string) {
        Set<Character> charsPresent = new HashSet<>();
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < string.length(); i++) {
            if (!charsPresent.contains(string.charAt(i))) {
                stringBuilder.append(string.charAt(i));
                charsPresent.add(string.charAt(i));
            }
        }

        return stringBuilder.toString();
    }
    @GetMapping("/string-concat")
    public String minCat(@RequestParam(value="string1")String string1, 
                         @RequestParam(value="string2")String string2)
    {
        if (string1.length() == string2.length())
            return string1+string2;
        if (string1.length() > string2.length())
        {
            int diff = string1.length() - string2.length();
            return string1.substring(diff, string1.length()) + string2;
        } else
        {
            int diff = string2.length() - string1.length();
            return string1 + string2.substring(diff, string2.length());
        }
    }
}
